import os
import xlwt

# Writes names and values to the excel sheet given the row and column
def write_excel(names, values, sheet, row, col):
    
    ii = 0
    for name in names:
        sheet.write(row, col, name)
        sheet.write(row, col+1, int(values[ii]))
        ii = ii + 1
        row = row + 1



