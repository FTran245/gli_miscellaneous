import os
import xlwt

fp = 'C:/Users/francis_t\Desktop\FortuneCoin_1.0.3_V1.0.0_SB\WEB\WEB-fortuneCoin.1.0.3.installation\web/filtered'

#Skateboard_jet_games_Slot-RedHotTamales-mainline_CL322507_319_SB
# Declare list
filepath = []

# Iterate through the files and find all .json files
# NOTE: Python does not see the colon : as a string, it sees it as a delimiter
# So you can't include colons in strings (or file paths for that matter)
for root, dirs, files in os.walk(fp):
    for file in files:
        
        if file.endswith('.json'):
            # print(os.path.join(root, file))
            filepath.append(os.path.join(root, file))

print("Total .json files found in " + fp + ": ", len(filepath))

# Try to print to Excel
book = xlwt.Workbook()
sh = book.add_sheet("Sheet1")

c = 0
cutoff_ind = filepath[2].index('FortuneCoin_1.0.3_V1.0.0_SB') # Find the index to truncate the path (remove the C:\Users etc..)
le = len('FortuneCoin_1.0.3_V1.0.0_SB') # Add 1 for the extra blackslash - no need for some I guess

for jsonfile in filepath:
    c = c + 1
    sh.write(c, 0, jsonfile[cutoff_ind+le:]) # sh2.write(row, col, content)

# Save file as .xls (Excel)
book.save("JSON.xls")


