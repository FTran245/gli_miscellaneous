import os
import xlwt
import xml.etree.ElementTree as ET

# User defined
from xml_library import * # imports all functions from your file

fname = "xmlfile.xml"

# https://docs.python.org/2/library/xml.etree.elementtree.html
# Open the xml file and get the root
tree = ET.parse(fname)
root = tree.getroot()

# Will loop iteratively down the children and find PrizePay and print the tag (blue), and its attributes
# Now we need to find out how to extract a particular attribute
# Check out https://developer.rhino3d.com/guides/rhinopython/python-dictionaries/
# I think it says use child.value() or child.key() to access dictionary values? ..
# Since child.attrib returns a dictionary
# You can use either [] or .get() to access value in a dictionary, but how can we access the key ..
# for child in root.iter():
#    # print(child.tag, child.attrib['count'], child.attrib.get('count'))
#     print(child.tag, child.attrib)



values = [] # declare a list, you can't assign using indices like normal, you need to use append() (like C++ vector?)
names = []
row = 0
col = 0
n = 0

# Initialize Excel workbook
book = xlwt.Workbook()
sh = book.add_sheet('Sheet1')

# Use iter('PaytableData') to iterate through elements with that tag, loop through the paytables 
# Get the names, then get the pays
for ii in root.iter('PrizeInfo'):
    print(len(ii), ii.attrib.get('name'))
    prize_name = ii.attrib.get('name')
    sh.write(row, col, prize_name) # Write the name of the hand to Excel
    row = row + 1

    for k in ii.iter('Prize'):
        names.append(k.attrib.get('name'))
        
        for child in k:
            if child.get('value') is None:
                pass
            else: 
                values.append(child.get('value'))

    # Our function write_excel(names, values, sheet) writes just names and values to an Excel Sheet
    write_excel(names, values, sh, row, col) 
    
    # Reset the row and move the column over an extra tab for readability
    row = 0
    col = col + 3
    
    # Clear the lists. Apparently you can't really do this by reference
    names = []
    values = []

# Save to Excel
book.save('test.xls')











