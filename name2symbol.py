import os
import xlwt
import xlrd
from xlutils.copy import copy 

def n2s(ra, col, sh, sh_col, name, wb):
    
    reel = []

    for ii in range(1, ra):
        reel.append(sh.cell_value(ii, col)) # row col


    reel = [r.replace('Lucky Kirin', 's01') for r in reel]
    reel = [r.replace('Lucky Turtle', 's02') for r in reel]
    reel = [r.replace("Lucky Fish", "s03") for r in reel]
    reel = [r.replace("Dog Urn", "s04") for r in reel]
    reel = [r.replace("Dragon Bell", "s05") for r in reel]
    reel = [r.replace("Ace", "s06") for r in reel]
    reel = [r.replace("King", "s07") for r in reel]
    reel = [r.replace("Queen", "s08") for r in reel]
    reel = [r.replace("Jack", "s09") for r in reel]
    reel = [r.replace("Ten", "s10") for r in reel]
    reel = [r.replace("Nine", "s11") for r in reel]
    reel = [r.replace("Coin", "c01") for r in reel]
    reel = [r.replace("Dragon Head", "b01") for r in reel]
    reel = [r.replace("Wild", "w01") for r in reel]

    # book = xlwt.Workbook()
    # w = copy(wb)
    # w_sh = w.get_sheet(0)
    # wb = xlrd.open_workbook(name)
    # sheet = wb.add_sheet("Sheet1")

    # for jj in range(1, ra):
    #     w_sh.write(jj, col, reel[jj-1])
    
    # filen = name+".xls"
    # w.save(filen)

    return reel


def write_file(reels, ro, col):

    file = open("symbols.txt", "a")

    for jj in range (0, len(reels)):
        file.write(reels[jj]+"\n")

    file.write("\n")
    file.close()

# Not part of function
fname = "names.xls"
wb = xlrd.open_workbook(fname)
sh = wb.sheet_by_index(0)

reel1 = []
reel2 = []
reel3 = []
reel4 = []
reel5 = []

rr1 = 15
rr2 = 16
rr3 = 16
rr4 = 16
rr5 = 16

# BG_SS1
reel1 = n2s(rr1, 0, sh, 0, "BG_SS1", wb)
reel2 = n2s(rr2, 2, sh, 2, "BG_SS1", wb)
reel3 = n2s(rr3, 4, sh, 4, "BG_SS1", wb)
reel4 = n2s(rr4, 6, sh, 6, "BG_SS1", wb)
reel5 = n2s(rr5, 8, sh, 8, "BG_SS1", wb)

write_file(reel1, rr1, 0)
write_file(reel2, rr2, 0)
write_file(reel3, rr3, 0)
write_file(reel4, rr4, 0)
write_file(reel5, rr5, 0)


