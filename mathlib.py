import os
import xlwt
import xlrd

def get_sd(fp, savepath):

    # Reading
    os.chdir(fp)
    files = os.listdir()
    val = []
    n = 0

    # Writing
    book = xlwt.Workbook()
    SD_sheet = book.add_sheet("Sheet1")

    for name in files:
        # Read the Excel sheet
        #print(name)
        wb = xlrd.open_workbook(name)
        sh = wb.sheet_by_index(7)
        val.append(sh.cell_value(4, 5)) # read and store in list cell_value(row, col)

        # Write in the same loop to get the names
        SD_sheet.write(n, 0, name) # .write(row, col, value)
        SD_sheet.write(n, 1, val[n])

        print(val[n])
        n = n + 1
    
    # Be careful with directories
    book.save(savepath)



# This function gets the cycle, base game hit freq, and top award probability
def get_info(fp, savepath):
    
    # Reading
    os.chdir(fp)
    files = os.listdir()

    # Information lists
    cycle = []
    base_game_hit = []
    top_award_prob = []
    MaxRTP = []
    P_Odds = [] # Pennsylvania odds

    # Writing
    book = xlwt.Workbook()
    Jur_Info = book.add_sheet("Sheet1")
    
    Jur_Info.write(0, 1, "Cycle")
    Jur_Info.write(0, 3, "Base Game Hit")
    Jur_Info.write(0, 5, "Top Award Probability")
    Jur_Info.write(0, 7, "Max RTP")
    Jur_Info.write(0, 9, "Pennsylvania Odds")

    n = 1
    
    for name in files:
        # Read the Excel sheet
        #print(name)
        wb = xlrd.open_workbook(name)
        sh = wb.sheet_by_index(0) # Open to sheet 0

        # Reading values
        cycle.append(sh.cell_value(39, 5)) # read and store in list cell_value(row, col)
        base_game_hit.append(sh.cell_value(43, 5))
        top_award_prob.append(sh.cell_value(30, 5))
        MaxRTP.append(sh.cell_value(27, 5))
        P_Odds.append(sh.cell_value(33, 5))

        # Write in the same loop to get the names
        Jur_Info.write(n, 0, name) # .write(row, col, value)
        
        # Write cycle, base game hit frequency, top award probability 
        Jur_Info.write(n, 1, cycle[n-1])
        Jur_Info.write(n, 3, base_game_hit[n-1])
        Jur_Info.write(n, 5, top_award_prob[n-1])
        Jur_Info.write(n, 7, MaxRTP[n-1])
        Jur_Info.write(n, 9, P_Odds[n-1])
        n = n + 1
    
    # Be careful with directories
    book.save(savepath)



def get_vol_ind(fp, savepath_vol):
    
    # Reading
    os.chdir(fp)
    files = os.listdir()
    vol_ind = []
    n = 0

    # Writing
    book = xlwt.Workbook()
    SD_sheet = book.add_sheet("Sheet1")

    for name in files:
        # Read the Excel sheet
        #print(name)
        wb = xlrd.open_workbook(name)
        sh = wb.sheet_by_index(7)
        vol_ind.append(sh.cell_value(4, 0)) # read and store in list cell_value(row, col)

        # Write in the same loop to get the names
        SD_sheet.write(n, 0, name) # .write(row, col, value)
        SD_sheet.write(n, 1, vol_ind[n])

        print(vol_ind[n])
        n = n + 1
    
    # Be careful with directories
    book.save(savepath_vol)


# This function gets the Pennsylvania odds
def get_Pennsylvania(fp, savepath_Pennsylvania):
    
    # Reading
    os.chdir(fp) # like cd() in bash
    files = os.listdir() # this is like ls() in bash
    P_Odds = [] # delcare empty list
    n = 0

    # Writing
    book = xlwt.Workbook()
    Pennsylvania_Odds = book.add_sheet("Sheet1")

    for name in files:
        # Read the Excel sheet
        #print(name)
        wb = xlrd.open_workbook(name)
        sh = wb.sheet_by_index(0)
        P_Odds.append(sh.cell_value(33, 5)) # read and store in list cell_value(row, col)

        # Write in the same loop to get the names
        Pennsylvania_Odds.write(n, 0, name) # .write(row, col, value)
        Pennsylvania_Odds.write(n, 1, P_Odds[n])

        print(P_Odds[n])
        n = n + 1
    
    # Be careful with directories
    book.save(savepath_Pennsylvania)