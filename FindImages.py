import os # For file traversing
import xlwt # For writing Excel

# Root directory
fp = 'C:/Users/francis_t\Desktop\FortuneCoin_1.0.3_V1.0.0_SB\WEB\WEB-fortuneCoin.1.0.3.installation\web/filtered'

filepath = [] # Declare list

# Iterate through the files and find all .json files
# NOTE: Python does not see the colon : as a string, it sees it as a delimiter
# So you can't include colons in strings (or file paths for that matter)
for root, dirs, files in os.walk(fp):
    for file in files:
        if file.endswith('.jpeg') or file.endswith('.JPEG') or file.endswith('.jpg') or file.endswith('.JPG') or file.endswith('.png'):
            filepath.append(os.path.join(root, file))
            
            
print("Total .png and .jpeg images found:", len(filepath))

# Now write the list of images to an Excel
book = xlwt.Workbook()
sh2 = book.add_sheet("Sheet1")

# Determine the index to truncat the file path (not to include C:\Users..\etc..)
c = 0
cutoff = filepath[2].index('FortuneCoin_1.0.3_V1.0.0_SB')
le = len('FortuneCoin_1.0.3_V1.0.0_SB')

# Write to file
for fname in filepath:
    c = c + 1
    sh2.write(c, 0, fname[cutoff+le:]) # sh2.write(row, col, content)
    
# Save Excel
book.save("images.xls")

